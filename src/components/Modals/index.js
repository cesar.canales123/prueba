import "../../App.css";
import "./styles.css"

function Modals(props) {
  return (
    <div className="absolute w-full h-full grid place-cente dark:bg-slate-700">
      <div
        className="z-10 flex place-self-center flex-col rounded-xl bg-white p-5 pb-10 drop-shadow-3xl dark:bg-slate-800 dark:text-white"
        style={{ width: "min(600px, 90vw)", background: "rgba(243, 243, 243, 0.89)", border: "1px solid #000000" }}
      >
        <div className="flex justify-center items-center pb-5">
          <h2 className="font-black text-2xl ">Cómo jugar</h2>
        </div>
        <div className="modal overscroll-contain overflow-y-scroll sm:px-7">
          {props.children}
        </div>
        <div className="flex justify-center items-center pb-5">
         
        <button className="btnGame" onClick={() => {
              props.data(false);
            }}>
         !JUGAR¡
        </button>
        </div>
 
      </div>
    </div>
  );
}

export default Modals;
