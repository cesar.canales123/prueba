
import SwitchLigt from "../../assets/icons/switchLight.png";
import SwitchBlack from "../../assets/icons/switchBlack.png";

function BlackMode(props) {
  const handleClick = () => {
    console.log("darknes",props)
    props.darkness(!props.dark);
  };

  return (
    <div>

      <img src={props.dark ? SwitchBlack : SwitchLigt}  
              onClick={handleClick} 
      alt="Switch" style={{height: "30px", paddingTop: "2px"}} />

    </div>
  );
}
export default BlackMode;