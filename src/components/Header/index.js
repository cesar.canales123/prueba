import HelpIcon from "../../assets/icons/help.png";
import HelpIconWithe from "../../assets/icons/help-withe.png";
import SettingsIcon from "../../assets/icons/settings.png";
import SettingsIconWhite from "../../assets/icons/settings-withe.png"
import BlackSwitch from "./blackmode";
import './style.css';



function Header(props) {
    return (
        <div className="navbar flex w-100 justify-between items-center pt-5 py-3 sm:pt-3 text-black dark:text-white headerStyle dark:bg-slate-700">
            <img src={props.dark ?  HelpIconWithe: HelpIcon} alt="help" style={{display: 'flex', paddingLeft: "10px"}}
                onClick={() => {
                    props.data(true);
                }}
            />  
            <h1 className="text-3xl font-bold tracking-wider">WORDLE</h1>
           
            <div style={{display: 'flex', paddingRight: "10px"}}>
            <img src={props.dark ?  SettingsIconWhite: SettingsIcon} alt="Settings"/>
            <BlackSwitch darkness={props.darkness} dark={props.dark}/>
            </div>
        </div>
    );
}

export default Header;
