function Box(props) {
    let state = "text-black border-2 border-gray-300 dark:text-white dark:bg-slate-600";
    if (props.state === "green") state = "bg-green-500";
    if (props.state === "yellow") state = "bg-yellow-500";
    if (props.state === "black") state = "bg-zinc-500  dark:bg-gray-700 ";
    if (props.state === "withe") state = "bg-slate-50  dark:bg-gray-700";
    return (
        <div
            className={
                "w-8 h-8 sm:w-12 sm:h-12 grid place-items-center p-0 m-0 font-bold text-lg sm:text-2xl  " + state
            }
        >
            {props.value}
        </div>
    );
}

function Instructions() {
    return (
        <>
            <p className="text-left text-sm sm:text-base  font-regular mr-1">
                Adivina la palabra oculta en cinco intentos.
                <br />
                <br />
                Cada intento debe ser una palabra válida de 5 letras.
                <br />
                <br />
                Después de cada intento el color de las letras cambia para mostrar qué tan cerca estás de acertar la palabra.
            </p>
            <hr />
            <h3 className="text-left font-bold py-5">Ejemplos</h3>
            <div className="flex gap-1 justify-center">
                <Box value="G" state="green" />
                <Box value="A" state="withe"  />
                <Box value="T" state="withe" />
                <Box value="O" state="withe" />
                <Box value="S" state="withe" />
            </div>
            <p className="text-left text-sm sm:text-base py-2">
                La letra <b>G</b> está en la palabra y en la posición correcta.
            </p>
            <div className="flex gap-1 justify-center">
                <Box value="V" state="withe"/>
                <Box value="O" state="withe"/>
                <Box value="C" state="yellow" />
                <Box value="A" state="withe"/>
                <Box value="L" state="withe"/>
            </div>
            <p className="text-left text-sm sm:text-base py-2">
                La letra <b>O</b> no está en la palabra.
            </p>
            <div className="flex gap-1 justify-center">
                <Box value="C" state="withe"/>
                <Box value="A" state="withe"/>
                <Box value="N" state="withe"/>
                <Box value="T" state="withe"/>
                <Box value="O" state="black" />
            </div>
            <br></br>
            <div class="flex justify-center">
                Puede haber letras repetidas. Las pistas son independientes para cada letra.
            </div>
        
            <div class="flex justify-center">
                ¡Una palabra nueva cada 5 minutos!
            </div>
        </>
    );
}

export default Instructions;
