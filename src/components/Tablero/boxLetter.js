import { useEffect, useState } from "react";
import BackspaceIcon from "../../assets/icons/delete.png";


function BoxLetter(props) {
  const [state, setState] = useState("text-black border-2 border-gray-300 dark:bg-slate-800	dark:text-white rounded");

  useEffect(() => {
    setTimeout(() => {
      if (props.state === "C")
        setState("bg-correct text-white");
      if (props.state === "E")
        setState("bg-exist text-white");
      if (props.state === "N")
        setState("bg-wrong text-white dark:bg-slate-700	");
    }, 125 * props.pos);
  }, [props.state]);

  return (
    <div className={ "h-12 w-12 sm:w-14 sm:h-14 grid place-items-center p-0 m-0 font-bold text-2xl rounded-sm " + state }>
      {props.value === "DEL" ? <img src={BackspaceIcon} alt="backspace"></img>: props.value}
    </div>
  );
}

export default BoxLetter;
