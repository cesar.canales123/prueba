import { useEffect, useState } from "react";
import BackspaceIcon from "../../assets/icons/delete.png";
import './styles.css'

const keyboard = {
    line1: "QWERTYUIOP",
    line2: "ASDFGHJKL",
    line3: "ZXCVBNM",
};

let defaultLetters = [];

"abcdefjhijklmnopqrstuvwxyz".split("").forEach((i) => {
    defaultLetters[i] = "";
});

function Key(props) {
    const [style, setStyle] = useState("bg-gray-300 hover:bg-gray-300 dark:bg-slate-800	 dark:text-white dark:hover:bg-slate-600");

    const x = props.value.length === 1 ? "w-7 sm:w-10 " : "p-2 sm:p-4 ";
    const returnKey = () => {
        props.getKey(props.value);
    };

    useEffect(() => {
        setTimeout(() => {
            if (props.state === "C") setStyle("bg-correct text-white");
            if (props.state === "E") setStyle("bg-exist text-white");
            if (props.state === "N") setStyle("bg-wrong text-white dark:bg-gray-600");
        }, 250);
    }, [props.state]);

    return (
        <button
            className={
                x +
                style +
                " h-14 300 grid items-center rounded font-semibold cursor-pointer"
            }
            onClick={returnKey}
        >
            {props.value === "DEL" ? <img src={BackspaceIcon} alt="BackspaceIcon" /> : props.value}
        </button>
    );
}

function KeyBoard(props) {
    const [letters, setletters] = useState(defaultLetters);
    useEffect(() => {
        setletters(props.letters);
    }, [props.changed]);

    const keyHandler = (value) => {
        props.keyHandler(value);
    };
    return (
            <div className="flex flex-col items-center w-100 pb-5 keyboardback dark:bg-slate-700">
                <div className="flex gap-1 my-0.5 w-fit" style={{ color: "#56575E" }} >
                    {keyboard.line1.split("").map((value, key) => (
                        <Key
                            getKey={keyHandler}
                            value={value}
                            key={key}
                            state={letters[value]}
                        />
                    ))}
                </div>
                <div className="flex gap-1 my-0.5 w-fit" style={{ color: "#56575E" }}>
                    {keyboard.line2.split("").map((value, key) => (
                        <Key
                            getKey={keyHandler}
                            value={value}
                            key={key}
                            state={letters[value]}
                        />
                    ))}
                </div>
                <div className="flex gap-1 my-0.5 w-fit" style={{ color: "#56575E" }}>
                    <Key value="ENTER" getKey={keyHandler} />
                    {keyboard.line3.split("").map((value, key) => (
                        <Key
                            getKey={keyHandler}
                            value={value}
                            key={key}
                            state={letters[value]}
                        />
                    ))}
                    <Key value="DEL" getKey={keyHandler} />
                </div>
            </div>
       
    );
}

export default KeyBoard;
