import { useState, useEffect } from "react";
import KeyBoard from './components/Keyboard';
import Tablero from "./components/Tablero";
import Header from "./components/Header";
import Modals from "./components/Modals";
import Instructions from "./components/Instructions";
import Alert from "./components/Alert";
import './App.css';


function App(props) {
  const [letter, setLetter] = useState();
  const [clicked, setClicked] = useState(0);
  const [error, setError] = useState("");
  const [dark, setDark] = useState(false);
  const [changed, setChanged] = useState(false);
  const [letters, setLetters] = useState({});
  const [data, setData] = useState(false);
  const [settings, setSettings] = useState(false);



  const  darkHandler= (dark) => {
    if (dark) document.documentElement.classList.add("dark");
    else document.documentElement.classList.remove("dark");
  }; 


  useEffect(() => {
    darkHandler(dark);
  }, [dark]);

  const keyHandler = (letterValue) => {
    setLetter(letterValue);
    setClicked(clicked + 1);
  };

  const LettersHandler = (lettersValue) => {
    setLetters(lettersValue);
    setChanged(!changed);
  };


  useEffect(() => {
    window.addEventListener("keydown", onClickDown);

    return () => window.removeEventListener("keydown", onClickDown);
  });
  const onClickDown = (event) => {
    if (event.key === "Enter") {
      setLetter("ENTER");
      setClicked(clicked + 1);
    } else if (event.key === "Backspace") {
      setLetter("DEL");
      setClicked(clicked + 1);
    } else if ("abcdefghijklmnopqrstuvwxyz".includes(event.key.toLowerCase())) {
      setLetter(event.key.toUpperCase());
      setClicked(clicked + 1);
    }
  };



  return (
    <>
      <div className={"app dark:bg-slate-800"}>
        {data && (
          <Modals data={setData}>
            {" "}
            <Instructions />{" "}
          </Modals>
        )}
        {settings && (
          <Modals settings={setSettings}>
            {" "}
            <Instructions />{" "}
          </Modals>
        )}
        {error && <Alert>{error}</Alert>}
        <div className="board" >
          <Header data={setData} darkness={setDark} dark={dark} settings={setSettings} 
          style={{ padding: "19px", background: "#F3F3F3", borderRadius: "15px" }} />
          <Tablero
            letter={letter}
            clicks={clicked}
            letters={LettersHandler}
            error={setError}
          />
          <KeyBoard keyHandler={keyHandler} letters={letters} changed={changed} />
        </div>
      </div>
    </>

  );
}

export default App;
